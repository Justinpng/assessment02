(function() {
	var GroceryApp = angular.module("GroceryApp", []);

	// var SearchSvc = function($http, $q) {
	// 	var searchSvc = this;

	// 	SearchSvc.findProductByBrand = function(brand) {
	// 		var defer = $q.defer();
	// 		$http.post("/brand", { search: brand })
	// 			.then(function(result) {
	// 					defer.resolve(result.data);
	// 				})
	// 			.catch(function(err) {
	// 					defer.reject();
	// 				});
	// 		return (defer.promise);
	// 	}

//test to check if able to pull and show the results
   var GrocerySvc = function ($http, $q) {
        var grocerySvc = this;
       grocerySvc.showAllProducts = function() {
            var defer = $q.defer();
           $http.get("/grocery")
                .then(function(result) {
                    defer.resolve(result.data);
                }).catch(function(err) {
                    defer.reject(err);
                });
            return(defer.promise);
        };
   };
    GrocerySvc.$inject = ["$http", "$q"];
   var GroceryCtrl = function(GrocerySvc) {
        var groceryCtrl = this;
        
       GrocerySvc.showAllProducts()
            .then(function(result) {
                console.info(">>> groceries shown!", result);
                groceryCtrl.mydb = result;
            }).catch(function(err) {
                console.error("error = ", err);
            })
   }
    GroceryCtrl.$inject = ["GrocerySvc"];
    GroceryApp.service("GrocerySvc", GrocerySvc);
    GroceryApp.controller("GroceryCtrl", GroceryCtrl);
}) ();