//Load the libs
const q = require("q");
const path = require("path");
const mysql = require("mysql");
const bodyParser = require("body-parser");
const express = require("express");

// defining DB source
const pool = mysql.createPool({
    host: "localhost", port: 3306,
    user: "justin", password: "Aa@190679",
    database: "mydb",
    connectionLimit: 4
});

// making query with DB
const mkQuery = function(sql, pool) {
    return (function(){
        const defer = q.defer();
        const args = [];
        for (var i in arguments)
            args.push(arguments[i]);
        pool.getConnection(function(err, conn){
            if(err) {
                defer.reject(err);
                return;
            }
            conn.query(sql, args, function(err, result){
                if(err)
                    defer.reject(err);
                else
                    defer.resolve(result);
                conn.release();
            });
        });
        return (defer.promise);
    });
}
//Create an instance of the application
app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// setting command lines with mySql
const SHOW_ALL_PRODUCTS = "SELECT * from grocery_list ORDER BY ID LIMIT 20";
const showAllProducts = mkQuery(SHOW_ALL_PRODUCTS, pool);
app.get("/grocery", function(req,resp) {
    showAllProducts()
        .then(function(result) {
            resp.status(200);
            resp.type("application/json");
            resp.json(result);
        }).catch(function(err) {
            resp.status(500);
            resp.type("application/json");
            resp.json(err);
        });
});


app.use("libs", express.static(path.join(__dirname,"../bower_components")));
app.use(express.static(path.join(__dirname, "../client")));


const port = process.env.APP_PORT || 3002;
app.listen(port, function(){
    console.log("Application started at %s on port %d" ,
        new Date(), port);
});